/*
const data = [
    '..##.......',
    '#...#...#..',
    '.#....#..#.',
    '..#.#...#.#',
    '.#...##..#.',
    '..#.##.....',
    '.#.#.#....#',
    '.#........#',
    '#.##...#...',
    '#...##....#',
    '.#..#...#.#',
]
*/
const fs = require('fs');

const data = fs.readFileSync('data.txt', 'utf8').split('\n')
const width = data[0].length

// V1
/*
let [row, col] = [0,0]
let count = 0

while(row < data.length) {
    if (data[row][col] === '#') count++

    row++
    col = (col +3) % width
}

console.log(count)
*/

// V2
/*
const howManyTrees = (row = 0, col=0) => {
    if (row < data.length) {
        const isThereATree = data[row][col] === '#' ? 1 : 0
        return isThereATree + howManyTrees(row+1, (col +3) % width)
    } else {
        return 0
    }
} 

console.log(howManyTrees())
*/

// V3
const howManyTrees = (row = 0, col = 0) => {
    return row < data.length ? howManyTrees(row + 1, (col + 3) % width) + (data[row][col] === '#' ? 1 : 0) : 0
}

console.log(howManyTrees())