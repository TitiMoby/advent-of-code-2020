const dataFileName = "test_data.txt";
//const dataFileName = 'data.txt'

const fs = require("fs");
const data = fs.readFileSync(dataFileName, "utf8").split("\n");

const width = data[0].length;
/*
Right 1, down 1.
Right 3, down 1. (This is the slope you already checked.)
Right 5, down 1.
Right 7, down 1.
Right 1, down 2
*/
const directions = [
  [1, 1],
  [1, 3],
  [1, 5],
  [1, 7],
  [2, 1],
];

const howManyTrees = (dirRow, dirCol, row = 0, col = 0) =>
  row < data.length
    ? howManyTrees(dirRow, dirCol, row + dirRow, (col + dirCol) % width) +
      (data[row][col] === "#" ? 1 : 0)
    : 0;

const resultats = directions.map(([dirRow, dirCol]) =>
  howManyTrees(dirRow, dirCol)
);
const total = resultats.reduce(
  (multiplication, num) => multiplication * num,
  1
);

console.log(resultats);
console.log(total);

// version avec High Order Function ;)
const generateHowManyTrees = (dirRow, dirCol) => {
  const howManyTrees = (dirRow, dirCol, row = 0, col = 0) => {
      console.log(dirRow, dirCol, row, col)
    return row < data.length
      ? howManyTrees(dirRow, dirCol, row + dirRow, (col + dirCol) % width) +
        (data[row][col] === "#" ? 1 : 0)
      : 0;
  };

  return howManyTrees;
};

const counts = directions.map(([dirRow, dirCol]) =>
  generateHowManyTrees(dirRow, dirCol)()
);
const final = counts.reduce((multiplication, num) => multiplication * num, 1);

console.log(counts);
console.log(final);
