[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/TitiMoby/advent-of-code-2020)

# Advent of code 2020

Mon aventure perso pour Advent of Code 2020.
https://adventofcode.com/2020/

De manière générale, j'essaie de laisser mes différentes approches qui ont fonctionné.
Ca permet à ceux qui ne passe que par ce repository d'avoir quelques explications de mon raisonnement.
Sinon, je ne saurais que vous conseiller d'aller voir les vidéos que je tourne et publie dans [ma playlist YouTube](https://www.youtube.com/playlist?list=PLZoekfjBpp3V9X-G5iXmKzUq0Jd7W_XoM)
