let data;

function preload() {
  data = loadStrings('data.txt');
}

function setup() {
  createCanvas(400, 400);
  values = data.map(s => parseInt(s))
  // day 1 part 1
  values.forEach(x => values.forEach(y => {
    if (x + y == 2020) console.log('day1 part 1 ', x*y)
  }))
  
  // day 1 part 2
  values.forEach(
    x =>
    values.forEach(
      y =>
      values.forEach(z => {
        if (x + y + z == 2020) console.log('day 1 part 2 ', x * y * z)
      })
    )
  )
}

function draw() {
  background(220);
}