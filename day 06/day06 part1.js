const fileName = "data.txt";
//const fileName = "test_data.txt";

const fs = require("fs");
const data = fs.readFileSync(fileName, "utf8").trim().split("\n\n");

const groups = data.map((row) => row.split("\n"));

// comptons les lettres dans les groupes
const lettersCount = groups.map((group) => {
  const sets = group.map((line) => new Set(line));
  return [..."abcdefghijklmnopqrstuvwxyz"].filter((char) =>
    sets.some((set) => set.has(char))
  ).length;
});

console.log(lettersCount.reduce((sum, num) => sum+num, 0));
