const fs = require('fs');

const data = fs.readFileSync('data.txt', 'utf8').split('\n')
/*const data = [
    '1-3 a: abcde',
    '1-3 b: cdefg',
    '2-9 c: ccccccccc'
]*/
const regex = /(\d+)-(\d+) (\w): (\w*)/
resultat = data.map(row => {    
    const [_, first, second, controlChar, password] = row.match(regex)
    return {first, second, controlChar, password}
}).filter(({first, second, controlChar, password}) => {
    const arrayPassword = [...password]
    return arrayPassword[first-1] === controlChar ^ arrayPassword[second-1] === controlChar
}).length

console.log(resultat)