const fs = require('fs');

const data = fs.readFileSync('data.txt', 'utf8').split('\n')

const regex = /(\d+)-(\d+) (\w): (\w*)/
resultat = data.map(row => {    
    const [_, min, max, controlChar, password] = row.match(regex)
    return {min, max, controlChar, password}
}).filter(({min, max, controlChar, password}) => {
  const charCount = [...password].filter(c => c === controlChar).length
  return charCount >= parseInt(min) && charCount <= parseInt(max)
}).length

console.log(resultat)