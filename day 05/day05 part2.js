const fileName = "data.txt";
//const fileName = "test_data.txt";

const fs = require("fs");
const data = fs.readFileSync(fileName, "utf8").trim().split("\n");

const legend = { F: 0, B: 1, L: 0, R: 1 };

const boardindPassProcess = (pass) =>
  parseInt(
    pass.replace(/./g, (c) => legend[c]),
    2
  );

const seatIds = data.map(boardindPassProcess);

// il faut trouver les sièges espacés de 2 places
// l'usage du spread operator est là pour rester immutable ;)
const sortedIds = [...seatIds].sort((a, b) => a - b);

const solutions = sortedIds.filter(
  (seat, index) => sortedIds[index + 1] - seat === 2
);

// notre siège est donc entre les 2
console.log(solutions[0] + 1);

// amélioration possible
const seatSet = new Set(seatIds);
const solutions2 = seatIds.filter(
  (seat) => seatSet.has(seat + 2) && !seatSet.has(seat + 1)
);
console.log(solutions2[0] + 1);
