//const fileName = "data.txt";
const fileName = "test_data.txt";

const fs = require("fs");
const data = fs.readFileSync(fileName, "utf8").trim().split("\n");

// l'astuce pour ce puzzle vient du fait que les données sont en fait un codage binaire
// la partie gauche/droite se lit comme des nombres
// LLL = 0 = 000
// LLR = 1 = 001
// LRL = 2 = 010
// LRR = 3 = 001
//..
// RRR = 7 = 111
// ça va faciliter la suite et éviter des récursions ;)
const binaryRL = { L: 0, R: 1 };
const binaryFB = { F: 0, B: 1 };
const boardindPassProcess = (pass) => {
  // pour chaque boarding pass on sépare rangée et colonne
  const rowString = pass.slice(0, 7);
  const colString = pass.slice(7);

  // on converti les éléments en nombres en base binaire
  // const colBinary = [...colString].map((c) => binaryRL[c]).join("");
  const colBinary = colString.replace(/./g, (c) => binaryRL[c]);
  const rowBinary = rowString.replace(/./g, (c) => binaryFB[c]);

  // on converti les éléments en nombres en base décimale
  const col = parseInt(colBinary, 2)
  const row = parseInt(rowBinary, 2)

  // on calcule le seatId
  const seatId = row * 8 + col;

  return seatId;
};


const seatIds = data.map(boardindPassProcess);
// on prend le plus grand
console.log(Math.max(...seatIds));
