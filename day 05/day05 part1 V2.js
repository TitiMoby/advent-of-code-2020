const fileName = "data.txt";
//const fileName = "test_data.txt";

const fs = require("fs");
const data = fs.readFileSync(fileName, "utf8").trim().split("\n");

// V2
// Le codage fourni et la réponse attendue impliquent le phénomène suivant.
// prenons un boarding pass : BFFFBBFRRR
// le codage binaire des row donne : BFFFBBF => 1000110 soit 70 en décimal
// le codage binaire des col donne : RRR => 111 => soit 7 en décimal
// si on considère que l'on a row * 8 équivalent à row << 3 (décalage binaire)
// on aura notre row * 8 qui devient 1000110000 soit 3 '0' à la fin
// comme on ajoute les col qui ne feront jamais plus de 3 positions binaires, on peut déduire
// que BFFFBBFRRR => 1000110111 soit 567 qui correspond bien à : 70 * 8 + 7
const legend = { F: 0, B: 1, L: 0, R: 1 };

const boardindPassProcess = (pass) =>
  parseInt(
    pass.replace(/./g, (c) => legend[c]),
    2
  );

const seatIds = data.map(boardindPassProcess);
// on prend le plus grand
console.log(Math.max(...seatIds));
