const fileName = "data.txt";
//const fileName = "test_data.txt";

const fs = require("fs");
const data = fs.readFileSync(fileName, "utf8").split("\n\n");

const requiredFields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];

const keys = data.map(
  (entry) =>
    new Set(
      entry
        .replace(/\n/g, " ")
        .split(" ")
        .map((keyValue) => keyValue.split(":")[0])
    )
);

console.log(
  keys.filter((keySet) => requiredFields.every((key) => keySet.has(key))).length
);
