const fileName = "data.txt";
//const fileName = "test_data.txt";

const fs = require("fs");
const data = fs.readFileSync(fileName, "utf8").split("\n\n");

const requiredFields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];

const pairs = data.map((entry) =>
  entry
    .replace(/\n/g, " ")
    .split(" ")
    .map((keyValue) => keyValue.split(":"))
    .reduce((obj, [key, value]) => ({ ...obj, [key]: value }), {})
);

// byr (Birth Year) - four digits; at least 1920 and at most 2002.
// iyr (Issue Year) - four digits; at least 2010 and at most 2020.
// eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
// hgt (Height) - a number followed by either cm or in:
// If cm, the number must be at least 150 and at most 193.
// If in, the number must be at least 59 and at most 76.
// hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
// ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
// pid (Passport ID) - a nine-digit number, including leading zeroes.
// cid (Country ID) - ignored, missing or not.

const hasAllKeys = (obj) => requiredFields.every((key) => key in obj);

// vérifie que la chaine num représente un entier compris entre min et max
const checkRange = (num, min, max) => {
  const number = parseInt(num);
  return min <= number && number <= max;
};

// ne fonctionne que pour des objets respectant hasAllKeys
const isValid = ({ byr, iyr, eyr, hgt, hcl, ecl, pid }) => {
  const byrValid = checkRange(byr, 1920, 2002);
  const iyrValid = checkRange(iyr, 2010, 2020);
  const eyrValid = checkRange(eyr, 2020, 2030);

  const hclValid = /^#[0-9a-f]{6}$/.test(hcl);
  const eclValid = /^(?:amb|blu|brn|gry|grn|hzl|oth)$/.test(ecl);
  const pidValid = /^\d{9}$/.test(pid);

  // première version
  /*let hgtValid = false;
  const hgtRegex = /^(\d+)(cm|in)$/;
  if (hgtRegex.test(hgt)) {
    const [_, height, unit] = hgt.match(hgtRegex);
    hgtValid =
      (unit === "cm" && checkRange(height, 150, 193)) ||
      (unit === "in" && checkRange(height, 59, 76));
  }*/
  // A partir de "première version", on pêut remplacer par cette magnifique regex
  // Rappelez vous, https://regex101.com  ;)
  const hgtValid = /^(((59|6[0-9]|7[0-6])in)|((1[5-8][0-9]|19[0-3])cm))$/.test(
    hgt
  );

  // petite astuce, on forme un objet clé/valeurs (merci javascript qui utilise le nom de variable comme clé)
  const results = {
    byrValid,
    iyrValid,
    eyrValid,
    hclValid,
    eclValid,
    pidValid,
    hgtValid,
  };

  // on retourne le fait que toutes les validations doivent être vraies pour valider l'ensemble
  return Object.values(results).every((bool) => bool);
};

console.log(
  pairs.filter((entry) => hasAllKeys(entry) && isValid(entry)).length
);
